# To jest miejsce od którego należy zacząć zadanie TDD
# litery alfabetu - a b c d e f g h i j k l m n o p q r s t u v w x y z
# litery kodu -     d e f g h i j k l m n o p q r s t u v w x y z
# przesunięcie kodu : 3
# tekst wejściowy : fosa
# tekst kodowany : irvd


import unittest

import string

def cezar(napis: str, klucz: int) -> str:
    alfabet = string.ascii_letters
    kod = alfabet[klucz:] + alfabet[:klucz]
    tabela = str.maketrans(alfabet, kod)
    return napis.translate(tabela)

class TestSzyfruCezar(unittest.TestCase):

  def test_kodowanie(self):
      self.assertEqual(cezar("fosa", 3), "irvd")

  def test_dekodowanie(self):
      self.assertEqual(cezar("irvd", -3), "fosa")